package com.labs.valtech.batterymonitor.main

import android.content.Intent
import android.app.IntentService
import android.util.Log
import android.content.BroadcastReceiver
import android.content.Context
import android.os.BatteryManager


/**
 * A constructor is required, and must call the super IntentService(String)
 * constructor with a name for the worker thread.
 */
class BatteryMonitorService : IntentService("Battery Monitor") {

    private var batteryChanged = true;
    private var batteryLevel = 0;
    private val broadCastReceiver = object: BroadcastReceiver()
    {
        override fun onReceive(context: Context?, intent: Intent?) {
            batteryLevel = intent!!.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
            batteryChanged = true;
        }
    }

    override fun onCreate() {
        super.onCreate()
        broadCastReceiver.goAsync()
    }

    /**
     * The IntentService calls this method from the default worker thread with
     * the intent that started the service. When this method returns, IntentService
     * stops the service, as appropriate.
     */
    override fun onHandleIntent(intent: Intent?) {

        try {
            if(batteryChanged){
                batteryChanged = false;
                Log.w("Update", "Battery update")
            }
            Thread.sleep(5000)
        } catch (e: InterruptedException) {
            // Restore interrupt status.
            Thread.currentThread().interrupt()
        }

    }

    override fun onDestroy() {
        super.onDestroy()
        broadCastReceiver.abortBroadcast()
    }
}